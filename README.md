# Powiększony nagłówek

To jest przykładowy tekst, który zawiera co najmniej trzy paragrafy. Oto drugi paragraf.

**Przykładowe pogrubienie** oraz *przykładowe kursywa*. Dodatkowo, ~~przykładowe przekreślenie~~.

> Oto przykładowy cytat.

1. Lista numerowana:
   1. Element 1
   2. Element 2
   3. Element 3

- Lista nienumerowana:
  - Element A
  - Element B
  - Element C

```python
# Przykładowy blok kodu programu
def przykladowa_funkcja():
    print("Hello, World!")
    print("To jest przykładowy blok kodu z co najmniej 3 liniami.")
    return 42
